import {WebSocket} from "vite";
import Data = WebSocket.Data;

export type TeamType = {
    "id": number,
    "teamName": string,
    "teamDescription": string,
    "expireTime": Data,
    "maxNum": number,
    "teamPassword": string,
    "teamStatus": number,
    "userId": number,
    "userAvatar": string,
    "userName": string,
    "ifMember": boolean,
};