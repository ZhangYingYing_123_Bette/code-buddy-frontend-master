export type UserType = {
    id: number;
    userName: string;
    userAccount: string;
    avatarUrl: string;
    gender: number;
    phone: string;
    email: string;
    wxAccount: string;
    qqAccount: string;
    tags: string[];
    intro: string;
};