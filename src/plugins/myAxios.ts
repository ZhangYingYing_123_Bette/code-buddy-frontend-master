import axios from 'axios';
import {showToast} from "vant";

const isDev = process.env.NODE_ENV === 'development';

const myAxios = axios.create({
    baseURL: isDev? 'http://localhost:8088/api' : 'https://user-hub-backend-113868-5-1327752756.sh.run.tcloudbase.com/api'
});

myAxios.defaults.withCredentials = true;

myAxios.interceptors.request.use(config => {
    // console.log('我要发请求了', config);
    return config;
}, error => {
    return Promise.reject(error);
});

myAxios.interceptors.response.use(response => {
    // console.log('我收到响应了', response.data);
    if (response.data.success) {
        return response.data;
    } else {
        // 全局处理失败的响应数据
        showToast(response.data.errMessage);
        if (response.data.errCode == 700) {
            // 未登录跳转到登录页面
            window.location.href = '/#/login';
        }
        return Promise.reject(response.data);
    }
}, error => {
    return Promise.reject(error);
});

export default myAxios;
