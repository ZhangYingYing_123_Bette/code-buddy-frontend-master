import Index from '../pages/Index.vue'
import Team from '../pages/Team.vue'
import UserInfo from '../pages/UserInfo.vue'
import Search from '../pages/Search.vue'
import Edit from '../pages/Edit.vue'
import Result from '../pages/Result.vue'
import Login from '../pages/Login.vue'
import SaveTeam from '../pages/SaveTeam.vue'
import Center from '../pages/Center.vue'
import MyTeam from '../pages/MyTeam.vue'
import TeamMember from '../pages/TeamMember.vue'

const routes = [
    {path: '/', component: Index, title: '找搭子'},
    {path: '/login', component: Login, title: '登录'},
    {path: '/team', component: Team, title: '组队'},
    {path: '/user-info', component: UserInfo, title: '个人信息'},
    {path: '/search', component: Search, title: '找搭子'},
    {path: '/edit', component: Edit, title: '编辑资料'},
    {path: '/result', component: Result, title: '找搭子'},
    {path: '/save-team', component: SaveTeam, title: '编辑队伍'},
    {path: '/center', component: Center, title: '个人中心'},
    {path: '/my-team', component: MyTeam, title: '我的队伍'},
    {path: '/team-member', component: TeamMember, title: '队伍成员'},
];

export default routes;
